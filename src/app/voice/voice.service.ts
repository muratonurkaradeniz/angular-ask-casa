import { Injectable } from "@angular/core";
import { DialogFlowService } from '../dialogflow.service';
import { Subject } from "rxjs";
import { FirebaseService } from "../firebase.service";

@Injectable()
export class VoiceService{
    
    public voiceMessageSubject : Subject<string> = new Subject<string>();
    
    constructor(private dfService : DialogFlowService,
                private firebaseService: FirebaseService){
        if(!this.dfService.initialized){
            this.dfService.initializeDialogflow().subscribe();
        }
     }


    sendVoiceMessage(query: any){
       
        console.log(query);
        // this.messageSubject.next({type: 'userMsg', text: query});
        this.dfService.getVoiceResponseFromFlask(query).subscribe(
                (result) => {
                //   console.log(result.json().botMsg)
                  this.voiceMessageSubject.next(result.json().botMsg)
                  let storageObject = {
                      botMsg : result.json().botMsgText,
                      userMsg : result.json().userMsg,
                      type: "voice",
                      messageID: new Date()
                  };
                  this.firebaseService.storeChatMessages(storageObject);
                //   let tempResObject = result.json().botMsg;
                //   setTimeout(() => {this.messageSubject.next(
                //       {
                //           type: 'botMsg',
                //           text: tempResObject
                //       });

                // }, 300)
            });
    }
}